package com.company.products.dto;

import lombok.Data;

@Data
public class CategoryDto {
    private Long id;
    private String name;
}
